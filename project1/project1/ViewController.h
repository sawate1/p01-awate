//
//  ViewController.h
//  project1
//
//  Created by Anshima on 23/01/17.
//  Copyright © 2017 shivani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *Label1;

-(IBAction)clicked:(id)app;

@end

