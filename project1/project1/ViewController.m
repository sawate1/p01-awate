//
//  ViewController.m
//  project1
//
//  Created by Anshima on 23/01/17.
//  Copyright © 2017 shivani. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize Label1;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)clicked:(id)app
{
    [Label1 setText:@"Shivani Awate"];
    [Label1 setFont:[UIFont fontWithName:@"Arial" size:20]];
    [Label1 setBackgroundColor:[UIColor grayColor]];
}

@end
